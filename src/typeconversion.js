var typeconversion = {};

(function() {
    'use strict';

    /**
     * Takes a number an reinterprets it as it were an 8 bit unsigned int, useful for
     * comparing Uint8 data against higher precison values.
     * @param {number} number - The number to reinterpret.
     * @return {number} - The number, cast to a Uint8 and back to a number
     */
    typeconversion.numberToUint8 = function(number) {
        return new Uint8Array([number])[0];
    };

    /**
     * Takes a number an reinterprets it as it were an 16 bit signed int, useful for
     * comparing Int16 data against higher precison values.
     * @param {number} number - The number to reinterpret.
     * @return {number} - The number, cast to a Int16 and back to a number
     */
    typeconversion.numberToInt16 = function(number) {
        return new Int16Array([number])[0];
    };

    /**
     * Takes a number an reinterprets it as it were an unsigned 16 bit signed int, useful for
     * comparing Uint16 data against higher precison values.
     * @param {number} number - The number to reinterpret.
     * @return {number} - The number, cast to a Uint16 and back to a number
     */
    typeconversion.numberToUint16 = function(number) {
        return new Uint16Array([number])[0];
    };

    /**
     * Takes a number an reinterprets it as it were an 32 bit signed int, useful for
     * comparing Int32 data against higher precison values.
     * @param {number} number - The number to reinterpret.
     * @return {number} - The number, cast to a Int32 and back to a number
     */
    typeconversion.numberToInt32 = function(number) {
        return new Int32Array([number])[0];
    };

    /**
     * Takes a number an reinterprets it as it were an unsigned 32 bit signed int, useful for
     * comparing Uint32 data against higher precison values.
     * @param {number} number - The number to reinterpret.
     * @return {number} - The number, cast to a Uint16 and back to a number
     */
    typeconversion.numberToUint32 = function(number) {
        return new Uint32Array([number])[0];
    };

    /**
     * Takes a number an reinterprets it as it were a 32-bit float, useful for
     * comparing 32-bit float data against higher precison values.
     * @param {number} number - The number to reinterpret.
     * @return {number} - The number, cast to a 32-bit float and back to a number
     */
    typeconversion.numberToFloat32 = function(number) {
        return new Float32Array([number])[0];
    };

    /**
     * Takes a number an reinterprets it as it were a 64-bit float, useful for
     * comparing 64-bit float data against different precsisions
     * @param {number} number - The number to reinterpret.
     * @return {number} - The number, cast to a 64-bit float and back to a number
     */
    typeconversion.numberToFloat64 = function(number) {
        return new Float64Array([number])[0];
    };

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = typeconversion;
    }
})();
