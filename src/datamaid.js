var datamaid = {};

var BASE_TYPES = ['byte', 'int', 'uint', 'int16', 'uint16', 'int32', 'uint32', 'float32', 'float64', 'string', 'url'];

if (typeof require !== 'undefined' && module.exports) {
    typeconversion = require('./typeconversion');
}

(function() {
    'use strict';

    /**
     * Makes data from OPeNDAP 'tidy'.
     * @param {string} variable - The id of the variable to tidy.
     * @param {json} ddsData - The OPeNDAP dds data as JSON
     * @param {array} dodsData - The OPeNDAP dods data for the specified variable
     * @param {boolean} removeMissing (optional) - Whether to remove missing data
     * @return {array} - Array of 'tidied' data objects
     */
    datamaid.tidyOPeNDAP = function(variable, ddsData, dodsData, removeMissing) {
        if (removeMissing === undefined) {
            removeMissing = false;
        }

        var variableInfo = getVariableInfo(variable, ddsData);

        if (variableInfo.type === 'Grid') {
            return tidyGridData(variable, variableInfo, dodsData, removeMissing);
        }
        else if (variableInfo.type === 'Sequence') {
            if (removeMissing === true) {
                throw new Error('Sequence variable type does not support removing missing data.');
            }

            return tidySequenceData(variable, variableInfo, dodsData, removeMissing);
        }
        else if (BASE_TYPES.indexOf(variableInfo.type.toLowerCase()) !== -1) {
            return tidyBaseTypeData(variable, variableInfo, dodsData, removeMissing);
        }
        else {
            throw new TypeError('Unrecognized variable type: ' + variableInfo.type);
        }
    };

    var tidyGridData = function(variable, variableInfo, dodsData, removeMissing) {
        var retVal = [];

        //Recursive function that builds the tidied data into the retVal array
        mapArray(variable, variableInfo, dodsData[0][0], dodsData[0].slice(1, dodsData[0].length), removeMissing, retVal);

        return retVal;
    };

    var tidySequenceData = function(variable, variableInfo, dodsData) {
        var retVal = [];

        var fieldNames = getSequenceFieldNames(variable, variableInfo);

        dodsData[0].forEach(function(sequenceEntry) {
            var tidyResult = {};

            sequenceEntry.forEach(function(element, index) {
                tidyResult[fieldNames[index]] = sequenceEntry[index];

                //Add any attributes
                Object.keys(variableInfo.attributes).forEach(function(attribute) {
                    tidyResult[attribute] = variableInfo.attributes[attribute];
                });
            });

            retVal.push(tidyResult);
        });

        return retVal;
    };

    var tidyBaseTypeData = function(variable, variableInfo, dodsData, removeMissing) {
        var retVal = [];

        if (removeMissing === true) {
            var missingValue = getMissingValue(variableInfo);
        }

        dodsData[0].forEach(function(value) {
            var tidyResult = {};

            tidyResult[variable] = value;

            //Add any attributes
            Object.keys(variableInfo.attributes).forEach(function(attribute) {
                tidyResult[attribute] = variableInfo.attributes[attribute];
            });

            if (missingValue !== undefined) {
                if (missingValue !== value) {
                    retVal.push(tidyResult);
                }
            }
            else {
                retVal.push(tidyResult);
            }
        });

        return retVal;
    };

    //http://stackoverflow.com/questions/15854425/iterate-over-a-javascript-array-without-using-nested-for-loops/15854485
    var mapArray = function(variable, variableInfo, dodsData, axis, removeMissing, resultArray, indices, depth) {
        if (!indices) {
            //Prefill our counters with 0s
            ////http://stackoverflow.com/questions/1295584/most-efficient-way-to-create-a-zero-filled-javascript-array
            indices = Array.apply(null, Array(variableInfo.array.dimensions.length)).map(Number.prototype.valueOf, 0);
        }

        if (!depth) {
            depth = 0;
        }

        if (removeMissing === true) {
            var missingValue = getMissingValue(variableInfo);
        }

        dodsData.forEach(function(entry, index) {
            if (index === 0) {
                //Reset the counter at this depth
                indices[depth] = 0;
            }

            if (Array.isArray(entry)) {
                mapArray(variable, variableInfo, entry, axis, removeMissing, resultArray, indices, depth + 1);
            }
            else {
                var tidyResult = {};

                tidyResult[variable] = entry;

                //Add any attributes
                Object.keys(variableInfo.attributes).forEach(function(attribute) {
                    tidyResult[attribute] = variableInfo.attributes[attribute];
                });

                variableInfo.array.dimensions.forEach(function(dimensionName, dimensionIndex) {
                    tidyResult[dimensionName] = axis[dimensionIndex][indices[dimensionIndex]];
                });

                if (missingValue !== undefined) {
                    if (missingValue !== entry) {
                        resultArray.push(tidyResult);
                    }
                }
                else {
                    resultArray.push(tidyResult);
                }
            }

            //Update index counter
            indices[depth] = indices[depth] + 1;
        });
    };

    var getVariableInfo = function(variable, ddsData) {
        return ddsData[variable];
    };

    var getDataType = function(variableInfo) {
        if (variableInfo.array !== undefined) {
            if (variableInfo.array.type !== undefined) {
                return variableInfo.array.type.toLowerCase();
            }
        }

        if (variableInfo.type !== undefined) {
            return variableInfo.type.toLowerCase();
        }

        return undefined;
    };

    var getMissingValue = function(variableInfo) {
        if (variableInfo.attributes.missing_value !== undefined) {
            //It is possible for the 'missing_value' number to be stored as a value with higher
            //resolution than the data itself, this makes comparing to the missing_value impossible
            //without first removing that extra resolution
            var dataType = getDataType(variableInfo);

            if (dataType === 'byte') {
                return typeconversion.numberToUint8(variableInfo.attributes.missing_value);
            }
            else if (dataType === 'int' || dataType === 'int32') {
                return typeconversion.numberToInt32(variableInfo.attributes.missing_value);
            }
            else if (dataType === 'uint' || dataType === 'uint32') {
                return typeconversion.numberToUint32(variableInfo.attributes.missing_value);
            }
            else if (dataType === 'int16') {
                return typeconversion.numberToInt16(variableInfo.attributes.missing_value);
            }
            else if (dataType === 'uint16') {
                return typeconversion.numberToUint16(variableInfo.attributes.missing_value);
            }
            else if (dataType === 'float32') {
                return typeconversion.numberToFloat32(variableInfo.attributes.missing_value);
            }
            else if (dataType === 'float64') {
                return typeconversion.numberToFloat64(variableInfo.attributes.missing_value);
            }
            else {
                return variableInfo.attributes.missing_value;
            }
        }
        else {
            return undefined;
        }
    };

    var getSequenceFieldNames = function(variable, variableInfo) {
        //Fields with are inherent in dds data, which are not part of the sequence data
        var reservedFields = ['type', 'attributes', 'name', 'id'];

        //We need to figure out the names and order of the variables in the sequence data
        var fieldNames = [];

        //http://stackoverflow.com/questions/8312459/iterate-through-object-properties
        for (var property in variableInfo) {
            if (variableInfo.hasOwnProperty(property)) {
                if (reservedFields.indexOf(property) === -1) {
                    fieldNames.push(property);
                }
            }
        }

        return fieldNames;
    };

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = datamaid;
    }
})();
