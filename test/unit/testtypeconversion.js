describe('type conversion functions', function() {
    //The necessary require statements for testing against node
    if (typeof require !== 'undefined' && module.exports) {
        typeconversion = require('../../src/typeconversion');
    }

    describe('numberToUint8', function() {
        var MAX_UINT_8 = Math.pow(2, 8) - 1;

        it('reinterprets a number as an 8 bit unsigned int', function() {
            expect(typeconversion.numberToUint8(MAX_UINT_8 + 1)).toBe(0);
        });

        it('preserves 8 bit unsigned int representible values', function() {
            expect(typeconversion.numberToUint8(MAX_UINT_8)).toBe(MAX_UINT_8);
        });
    });

    describe('numberToInt16', function() {
        var MAX_INT_16 = Math.pow(2, 15) - 1;
        var MIN_INT_16 = -Math.pow(2, 15);

        it('reinterprets a number as a 16 bit signed int', function() {
            expect(typeconversion.numberToInt16(MAX_INT_16 + 1)).toBe(MIN_INT_16);
        });

        it('preserves 16 bit signed int representible values', function() {
            expect(typeconversion.numberToInt16(MAX_INT_16)).toBe(MAX_INT_16);
            expect(typeconversion.numberToInt16(MIN_INT_16)).toBe(MIN_INT_16);
        });
    });

    describe('numberToUint16', function() {
        var MAX_UINT_16 = Math.pow(2, 16) - 1;

        it('reinterprets a number as a 16 bit unsigned int', function() {
            expect(typeconversion.numberToUint16(MAX_UINT_16 + 1)).toBe(0);
        });

        it('preserves 16 bit unsigned int representible values', function() {
            expect(typeconversion.numberToUint16(MAX_UINT_16)).toBe(MAX_UINT_16);
        });
    });

    describe('numberToInt32', function() {
        var MAX_INT_32 = Math.pow(2, 31) - 1;
        var MIN_INT_32 = -Math.pow(2, 31);

        it('reinterprets a number as a 32 bit signed int', function() {
            expect(typeconversion.numberToInt32(MAX_INT_32 + 1)).toBe(MIN_INT_32);
        });

        it('preserves 32 bit signed int representible values', function() {
            expect(typeconversion.numberToInt32(MAX_INT_32)).toBe(MAX_INT_32);
            expect(typeconversion.numberToInt32(MIN_INT_32)).toBe(MIN_INT_32);
        });
    });

    describe('numberToUint32', function() {
        var MAX_UINT_32 = Math.pow(2, 32) - 1;

        it('reinterprets a number as a 32 bit unsigned int', function() {
            expect(typeconversion.numberToUint32(MAX_UINT_32 + 1)).toBe(0);
        });

        it('preserves 32 bit unsigned int representible values', function() {
            expect(typeconversion.numberToUint32(MAX_UINT_32)).toBe(MAX_UINT_32);
        });
    });

    describe('numberToFloat32', function() {
        it('reinterprets a number as a 32 bit float', function() {
            expect(typeconversion.numberToFloat32(-1e+34)).toBe(-9.999999790214768e+33);
        });

        it('preserves 32 bit float representible values', function() {
            expect(typeconversion.numberToFloat32(10)).toBe(10);
        });
    });

    describe('numberToFloat64', function() {
        it('reinterprets a number as a 64 bit float', function() {
            expect(typeconversion.numberToFloat64((1 + (1 - Math.pow(2, -52))) * Math.pow(2, 1023))).toBe(1.7976931348623157e+308);
        });

        it('preserves 64 bit float representible values', function() {
            expect(typeconversion.numberToFloat64(10)).toBe(10);
        });
    });
});
