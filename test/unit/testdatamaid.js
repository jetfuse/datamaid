describe('tidyOPeNDAP', function() {
    //The necessary require statements for testing against node
    if (typeof require !== 'undefined' && module.exports) {
        datamaid = require('../../src/datamaid');
    }

    describe('tidying Grid data', function() {
        var grid_dds = {type: 'Dataset',
            'VAR': {
                attributes: {units: 'test unit'},
                array: {
                    dimensions: ['AXIS1', 'AXIS2', 'AXIS3'],
                    shape: [3, 4, 2],
                },
                id: 'VAR',
                name: 'VAR',
                maps: {
                    'AXIS1': axis1_dds,
                    'AXIS2': axis2_dds,
                    'AXIS3': axis3_dds,
                },
                type: 'Grid',
            },
        };

        var axis1_dds = {
            attributes: {},
            dimensions: ['AXIS1'],
            id: 'AXIS1',
            name: 'AXIS1',
            shape: [3],
            type: 'Float64',
        };

        var axis2_dds = {
            attributes: {},
            dimensions: ['AXIS2'],
            id: 'AXIS2',
            name: 'AXIS2',
            shape: [4],
            type: 'Float64',
        };

        var axis3_dds = {
            attributes: {},
            dimensions: ['AXIS3'],
            id: 'AXIS3',
            name: 'AXIS3',
            shape: [2],
            type: 'Float64',
        };

        var grid_data = [[
            [
                [
                    [1, 2],
                    [3, 4],
                    [5, 6],
                    [7, 8],
                ],
                [
                    [10, 20],
                    [30, 40],
                    [50, 60],
                    [70, 80],
                ],
                [
                    [100, 200],
                    [300, 400],
                    [500, 600],
                    [700, 800],
                ],
            ],
            [1000, 2000, 3000],
            [10000, 20000, 30000, 40000],
            [100000, 200000],
        ]]; //Dap data is always wrapped in an outer array

        it('should tidy grid values', function() {
            var tidyResult = datamaid.tidyOPeNDAP('VAR', grid_dds, grid_data);

            var data = grid_data[0][0];

            var axis1 = grid_data[0][1];
            var axis2 = grid_data[0][2];
            var axis3 = grid_data[0][3];

            expect(tidyResult.length).toBe(24);

            axis1.forEach(function(axis1value, axis1index) {
                axis2.forEach(function(axis2value, axis2index) {
                    axis3.forEach(function(axis3value, axis3index) {
                        expect(tidyResult).toContain({'VAR': data[axis1index][axis2index][axis3index], 'units': 'test unit', 'AXIS1': axis1value, 'AXIS2': axis2value, 'AXIS3': axis3value});
                    });
                });
            });
        });

        it('should filter missing 64-bit values', function() {
            var float64_grid_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: -1e34},
                    array: {
                        dimensions: ['AXIS1', 'AXIS2', 'AXIS3'],
                        shape: [1, 1, 4],
                        type: 'Float64',
                    },
                    id: 'VAR',
                    name: 'VAR',
                    maps: {
                        'AXIS1': float64_axis1_dds,
                        'AXIS2': float64_axis2_dds,
                        'AXIS3': float64_axis3_dds,
                    },
                    type: 'Grid',
                },
            };

            var float64_axis1_dds = {
                attributes: {},
                dimensions: ['AXIS1'],
                id: 'AXIS1',
                name: 'AXIS1',
                shape: [1],
                type: 'Float64',
            };

            var float64_axis2_dds = {
                attributes: {},
                dimensions: ['AXIS2'],
                id: 'AXIS2',
                name: 'AXIS2',
                shape: [1],
                type: 'Float64',
            };

            var float64_axis3_dds = {
                attributes: {},
                dimensions: ['AXIS3'],
                id: 'AXIS3',
                name: 'AXIS3',
                shape: [4],
                type: 'Float64',
            };

            var float64_grid_data = [[
                [
                    [
                        [1, 1.234, -1e34, 1e34],
                    ],
                ],
                [1],
                [2],
                [3, 4, 5, 6],
            ]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', float64_grid_dds, float64_grid_data, true);

            expect(tidyResult.length).toBe(3);

            expect(tidyResult).toContain({'VAR': 1, 'AXIS1': 1, 'AXIS2': 2, 'AXIS3': 3, missing_value: -1e34});
            expect(tidyResult).toContain({'VAR': 1.234, 'AXIS1': 1, 'AXIS2': 2, 'AXIS3': 4, missing_value: -1e34});
            expect(tidyResult).toContain({'VAR': 1e34, 'AXIS1': 1, 'AXIS2': 2, 'AXIS3': 6, missing_value: -1e34});
        });

        it('should filter missing 32-bit values', function() {
            var float32_grid_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: -1e34},
                    array: {
                        dimensions: ['AXIS1', 'AXIS2', 'AXIS3'],
                        shape: [1, 1, 4],
                        type: 'Float32',
                    },
                    id: 'VAR',
                    name: 'VAR',
                    maps: {
                        'AXIS1': float32_axis1_dds,
                        'AXIS2': float32_axis2_dds,
                        'AXIS3': float32_axis3_dds,
                    },
                    type: 'Grid',
                },
            };

            var float32_axis1_dds = {
                attributes: {},
                dimensions: ['AXIS1'],
                id: 'AXIS1',
                name: 'AXIS1',
                shape: [1],
                type: 'Float32',
            };

            var float32_axis2_dds = {
                attributes: {},
                dimensions: ['AXIS2'],
                id: 'AXIS2',
                name: 'AXIS2',
                shape: [1],
                type: 'Float32',
            };

            var float32_axis3_dds = {
                attributes: {},
                dimensions: ['AXIS3'],
                id: 'AXIS3',
                name: 'AXIS3',
                shape: [4],
                type: 'Float32',
            };

            var float32_grid_data = [[
                [
                    [
                        [1, 1.234, -9.999999790214768e+33, 1e+16],
                    ],
                ],
                [1],
                [2],
                [3, 4, 5, 6],
            ]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', float32_grid_dds, float32_grid_data, true);

            expect(tidyResult.length).toBe(3);

            expect(tidyResult).toContain({'VAR': 1, 'AXIS1': 1, 'AXIS2': 2, 'AXIS3': 3, missing_value: -1e34});
            expect(tidyResult).toContain({'VAR': 1.234, 'AXIS1': 1, 'AXIS2': 2, 'AXIS3': 4, missing_value: -1e34});
            expect(tidyResult).toContain({'VAR': 1e16, 'AXIS1': 1, 'AXIS2': 2, 'AXIS3': 6, missing_value: -1e34});
        });
    });

    describe('tidying Sequence data', function() {
        var sequence_dds = {type: 'Dataset',
            'VAR': {
                attributes: {units: 'test unit'},
                string_element: {
                    dimensions: [],
                    id: 'VAR.string_element',
                    name: 'string_element',
                    shape: [],
                    type: 'String',
                },
                float64_element: {
                    dimensions: [],
                    id: 'VAR.float64_element',
                    name: 'float64_element',
                    shape: [],
                    type: 'Float64',
                },
                id: 'VAR',
                name: 'VAR',
                type: 'Sequence',
            },
        };

        var sequence_data = [[['string element 1', 1], ['string element 2', 1.234], ['string element 3', -1e34], ['string element 4', 1e34]]]; //Dap data is always wrapped in an outer array

        it('should tidy sequence values', function() {
            var tidyResult = datamaid.tidyOPeNDAP('VAR', sequence_dds, sequence_data);

            expect(tidyResult.length).toBe(4);

            expect(tidyResult[0]).toEqual({string_element: 'string element 1', float64_element: 1., units: 'test unit'});
            expect(tidyResult[1]).toEqual({string_element: 'string element 2', float64_element: 1.234, units: 'test unit'});
            expect(tidyResult[2]).toEqual({string_element: 'string element 3', float64_element: -1e34, units: 'test unit'});
            expect(tidyResult[3]).toEqual({string_element: 'string element 4', float64_element: 1e34, units: 'test unit'});
        });

        it('should not allow removing missing', function() {
            var callWrapper = function() {
                datamaid.tidyOPeNDAP('VAR', sequence_dds, sequence_data, true);
            };

            expect(callWrapper).toThrowError('Sequence variable type does not support removing missing data.');
        });
    });

    describe('tidying Byte data', function() {
        it('should preserve byte values', function() {
            var byte_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [2],
                    type: 'Byte',
                },
            };

            var byte_data = [[0x00, 0xFF]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', byte_dds, byte_data);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: 0x00});
            expect(tidyResult[1]).toEqual({VAR: 0xff});
        });

        it('should filter missing byte values', function() {
            var byte_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: 0x0f},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [3],
                    type: 'Byte',
                },
            };

            var byte_data = [[0x00, 0x0f, 0xff]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', byte_dds, byte_data, true);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: 0x00, missing_value: 0x0f});
            expect(tidyResult[1]).toEqual({VAR: 0xff, missing_value: 0x0f});
        });
    });

    describe('tidying Int data', function() {
        it('should preserve int values', function() {
            var int_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [2],
                    type: 'Int',
                },
            };

            var int_data = [[Math.pow(2, 31) - 1, -Math.pow(2, 31)]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', int_dds, int_data);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: Math.pow(2, 31) - 1});
            expect(tidyResult[1]).toEqual({VAR: -Math.pow(2, 31)});
        });

        it('should filter missing int values', function() {
            var int_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: 7},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [3],
                    type: 'Int',
                },
            };

            var int_data = [[Math.pow(2, 31) - 1, 7, -Math.pow(2, 31)]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', int_dds, int_data, true);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: Math.pow(2, 31) - 1, missing_value: 7});
            expect(tidyResult[1]).toEqual({VAR: -Math.pow(2, 31), missing_value: 7});
        });
    });

    describe('tidying Uint data', function() {
        it('should preserve uint values', function() {
            var uint_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [2],
                    type: 'Uint',
                },
            };

            var uint_data = [[0, Math.pow(2, 32) - 1]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', uint_dds, uint_data);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: 0});
            expect(tidyResult[1]).toEqual({VAR: Math.pow(2, 32) - 1});
        });

        it('should filter missing uint values', function() {
            var uint_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: 17},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [3],
                    type: 'Uint',
                },
            };

            var uint_data = [[0, Math.pow(2, 32) - 1]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', uint_dds, uint_data, true);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: 0, missing_value: 17});
            expect(tidyResult[1]).toEqual({VAR: Math.pow(2, 32) - 1, missing_value: 17});
        });
    });

    describe('tidying Int16 data', function() {
        it('should preserve int16 values', function() {
            var int16_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [2],
                    type: 'Int16',
                },
            };

            var int16_data = [[Math.pow(2, 15) - 1, -Math.pow(2, 15)]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', int16_dds, int16_data);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: Math.pow(2, 15) - 1});
            expect(tidyResult[1]).toEqual({VAR: -Math.pow(2, 15)});
        });

        it('should filter missing int16 values', function() {
            var int16_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: 27},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [3],
                    type: 'Int16',
                },
            };

            var int16_data = [[Math.pow(2, 15) - 1, 27, -Math.pow(2, 15)]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', int16_dds, int16_data, true);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: Math.pow(2, 15) - 1, missing_value: 27});
            expect(tidyResult[1]).toEqual({VAR: -Math.pow(2, 15), missing_value: 27});
        });
    });

    describe('tidying Uint16 data', function() {
        it('should preserve uint16 values', function() {
            var uint16_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [2],
                    type: 'Uint16',
                },
            };

            var uint16_data = [[0, Math.pow(2, 32) - 1]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', uint16_dds, uint16_data);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: 0});
            expect(tidyResult[1]).toEqual({VAR: Math.pow(2, 32) - 1});
        });

        it('should filter missing uint16 values', function() {
            var uint16_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: 37},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [3],
                    type: 'Uint16',
                },
            };

            var uint16_data = [[0, 37, Math.pow(2, 32) - 1]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', uint16_dds, uint16_data, true);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: 0, missing_value: 37});
            expect(tidyResult[1]).toEqual({VAR: Math.pow(2, 32) - 1, missing_value: 37});
        });
    });

    describe('tidying Int32 data', function() {
        it('should preserve int32 values', function() {
            var int32_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [2],
                    type: 'Int32',
                },
            };

            var int32_data = [[Math.pow(2, 31) - 1, -Math.pow(2, 31)]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', int32_dds, int32_data);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: Math.pow(2, 31) - 1});
            expect(tidyResult[1]).toEqual({VAR: -Math.pow(2, 31)});
        });

        it('should filter missing int32 values', function() {
            var int32_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: 47},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [3],
                    type: 'Int32',
                },
            };

            var int32_data = [[Math.pow(2, 31) - 1, 47, -Math.pow(2, 31)]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', int32_dds, int32_data, true);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: Math.pow(2, 31) - 1, missing_value: 47});
            expect(tidyResult[1]).toEqual({VAR: -Math.pow(2, 31), missing_value: 47});
        });
    });

    describe('tidying Uint32 data', function() {
        it('should preserve uint32 values', function() {
            var uint32_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [2],
                    type: 'Uint32',
                },
            };

            var uint32_data = [[0, Math.pow(2, 32) - 1]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', uint32_dds, uint32_data);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: 0});
            expect(tidyResult[1]).toEqual({VAR: Math.pow(2, 32) - 1});
        });

        it('should filter missing uint32 values', function() {
            var int32_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: 57},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [3],
                    type: 'Uint32',
                },
            };

            var int32_data = [[0, 57, Math.pow(2, 32) - 1]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', int32_dds, int32_data, true);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: 0, missing_value: 57});
            expect(tidyResult[1]).toEqual({VAR: Math.pow(2, 32) - 1, missing_value: 57});
        });
    });

    describe('tidying Float64 data', function() {
        it('should preserve 64-bit values', function() {
            var float64_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [4],
                    type: 'Float64',
                },
            };

            var float64_data = [[1, 1.234, -1e+34, 1e+34]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', float64_dds, float64_data);

            expect(tidyResult.length).toBe(4);

            expect(tidyResult[0]).toEqual({VAR: 1});
            expect(tidyResult[1]).toEqual({VAR: 1.234});
            expect(tidyResult[2]).toEqual({VAR: -1e34});
            expect(tidyResult[3]).toEqual({VAR: 1e34});
        });

        it('should filter missing 64-bit values', function() {
            var float64_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: -1e34},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [4],
                    type: 'Float64',
                },
            };

            var float64_data = [[1, 1.234, -1e+34, 1e+34]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', float64_dds, float64_data, true);

            expect(tidyResult.length).toBe(3);

            expect(tidyResult[0]).toEqual({VAR: 1, missing_value: -1e34});
            expect(tidyResult[1]).toEqual({VAR: 1.234, missing_value: -1e34});
            expect(tidyResult[2]).toEqual({VAR: 1e34, missing_value: -1e34});
        });
    });

    describe('tidying Float32 data', function() {
        it('should preserve 32-bit values', function() {
            var float32_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [4],
                    type: 'Float32',
                },
            };

            var float32_data = [[1, 1.234, -1e+16, 1e+16]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', float32_dds, float32_data);

            expect(tidyResult.length).toBe(4);

            expect(tidyResult[0]).toEqual({VAR: 1});
            expect(tidyResult[1]).toEqual({VAR: 1.234});
            expect(tidyResult[2]).toEqual({VAR: -1e16});
            expect(tidyResult[3]).toEqual({VAR: 1e16});
        });

        it('should filter missing 32-bit values', function() {
            var float32_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {missing_value: -1e34}, //This should be cast to 32-bit
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [4],
                    type: 'Float32',
                },
            };

            var float32_data = [[1, 1.234, -9.999999790214768e+33, 1e+16]];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', float32_dds, float32_data, true);

            expect(tidyResult.length).toBe(3);

            expect(tidyResult[0]).toEqual({VAR: 1, missing_value: -1e34});
            expect(tidyResult[1]).toEqual({VAR: 1.234, missing_value: -1e34});
            expect(tidyResult[2]).toEqual({VAR: 1e16, missing_value: -1e34});
        });
    });

    describe('tidying String data', function() {
        it('should preserve strings', function() {
            var string_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [2],
                    type: 'String',
                },
            };

            var string_data = [['ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 0123456789 ;-_', '_-; 9876543210 zyxwvutsrqponmlkjihgfedcba ZYXWVUTSRQPONMLKJIHGFEDCBA']];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', string_dds, string_data);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 0123456789 ;-_'});
            expect(tidyResult[1]).toEqual({VAR: '_-; 9876543210 zyxwvutsrqponmlkjihgfedcba ZYXWVUTSRQPONMLKJIHGFEDCBA'});
        });
    });

    describe('tidying Url data', function() {
        it('should preserve strings', function() {
            var string_dds = {type: 'Dataset',
                'VAR': {
                    attributes: {},
                    dimensions: ['VAR'],
                    id: 'VAR',
                    name: 'VAR',
                    shape: [2],
                    type: 'Url',
                },
            };

            var string_data = [['http://test1.com', 'http://test2.com']];

            var tidyResult = datamaid.tidyOPeNDAP('VAR', string_dds, string_data);

            expect(tidyResult.length).toBe(2);

            expect(tidyResult[0]).toEqual({VAR: 'http://test1.com'});
            expect(tidyResult[1]).toEqual({VAR: 'http://test2.com'});
        });
    });
});
