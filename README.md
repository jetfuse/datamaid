# datamaid

JavaScript library for taking data and making it [tidy](http://vita.had.co.nz/papers/tidy-data.pdf) (caution, PDF link) which can aid in using the data in tools such a [D3.js](http://d3js.org/).

## Installation

For use with [Node.js](https://nodejs.org/en/), simply install using npm:

```sh
$ npm install datamaid
```

For use with a browser, the source must be checked out from [the git repository](https://bitbucket.org/jetfuse/datamaid). After checking out the source code, install the dev dependencies and use npm to compile the source:

```sh
$ npm install
$ npm run build
```

This should generate `datamaid.js` and the minified `datamaid.min.js` in the project root. These files can used directly by the browser.

## Use

### OPeNDAP

[OPeNDAP](http://www.opendap.org/) data can be tidied with the `datamaid.tidyOPeNDAP` method. Load the data using [jsdap](https://www.npmjs.com/package/jsdap) (or other JavaScript OPeNDAP client). Both the DDS and DODS data are required for tidying.

For example, to load and tidy WSPD data from the [OPeNDAP server test site](http://test.opendap.org/) using jsdap and [jQuery](https://jquery.com/):

```js
function load() {
	var baseUrl = 'http://test.pydap.org/coads.nc';
	var varName = 'WSPD';

	$.when(
		opendapLoadDatasetDescriptionPromise(baseUrl),
		opendapLoadDataPromise(baseUrl, varName)
	).done(function(ddsData, varData) {
		var tidyData = datamaid.tidyOPeNDAP(varName, ddsData, varData, true);
	});
}

function opendapLoadDatasetDescriptionPromise(url) {
	var deferred = $.Deferred();

	loadDataset(url, function(data) {
		deferred.resolve(data);
	}, '/proxy/');

	return deferred.promise();
}

function opendapLoadDataPromise(url, variable) {
	var deferred = $.Deferred();

	var dodsUrl = url + '.dods?' + variable;

	loadData(dodsUrl, function(data) {
		deferred.resolve(data);
	}, '/proxy/');

	return deferred.promise();
}
```

## Examples

More detailed examples can be found in the `examples` folder, `examples/README.md` covers how to run them.

## Development

The src is all inside the `src` directory. To get started with development install the dev dependencies:

```sh
$ npm install
```

You may wish to link [eslint](http://eslint.org/) into your path for JavaScript linting, assuming development inside a [nodeenv](https://github.com/ekalinin/nodeenv):

```sh
$ ln -s --relative node_modules/eslint/bin/eslint.js ../bin/eslint
```

Unit tests are written with [Jasmine](http://jasmine.github.io/) and run by the [Karma](http://karma-runner.github.io) test runner. Tests are run against Firefox, Chrome, and Node.js. They can be run using npm:

```sh
$ npm run test
```
