var baseUrl = 'http://test.pydap.org/coads.nc';

function load() {
	var varName = 'WSPD';

	$.when(
		opendapLoadDatasetDescriptionPromise(baseUrl),
		opendapLoadDataPromise(baseUrl, varName, [[0, 11], [5, 84], [5, 174]]) //Dimensions: TIME, COADSY, COADSX
	).done(function(ddsData, varData) {
		var tidyData = datamaid.tidyOPeNDAP(varName, ddsData, varData, true);

		animatedContour(varName, tidyData);
	});
}

function opendapLoadDatasetDescriptionPromise(url) {
	var deferred = $.Deferred();

	jsdap.loadDataset(url, function(data) {
		deferred.resolve(data);
	}, '/proxy/');

	return deferred.promise();
}

function opendapLoadDataPromise(url, variable, dimensions) {
	var deferred = $.Deferred();

	var dodsUrl = url + '.dods?' + variable;

	if (dimensions !== undefined) {
		var dimensionString = '';

		dimensions.forEach(function(dimensionIndices) {
			dimensionString += '[' + dimensionIndices.join(':') + ']';
		});

		dodsUrl += dimensionString;
	}

	jsdap.loadData(dodsUrl, function(data) {
		deferred.resolve(data);
	}, '/proxy/');

	return deferred.promise();
}

function animatedContour(varName, tidyData) {
	var width = 960;
	var height = 500;

	var animationDuration = 1000;
	var slideDuration = 5000;

	var numLevels = 20;

	var extents = d3.extent(tidyData, function(d) { return d[varName]; });
	var minValue = extents[0];
	var maxValue = extents[1];

	var contourLevels = d3.range(minValue, maxValue, (maxValue - minValue) / numLevels);

	var contourId = 0; //Crude method of generating contour ids

	var colors = d3.scale.linear().domain([minValue, maxValue]).range(['#99C2EB', '#002952']);

	var time = buildAxis(tidyData, 'TIME');
	var xAxis = buildAxis(tidyData, 'COADSX');
	var yAxis = buildAxis(tidyData, 'COADSY');

	//Precalculate the contours
	var contours = buildContours(varName, tidyData, time, xAxis, yAxis, contourLevels);

	//Set up the chart
	var chart = d3.select('.chart')
									.attr('width', width)
    							.attr('height', height)

	var x = d3.scale.linear().range([0, width]).domain([d3.min(xAxis), d3.max(xAxis)]);
  var y = d3.scale.linear().range([height, 0]).domain([d3.min(yAxis), d3.max(yAxis)]);

	function draw(timeIndex) {
		var contourLines = chart.selectAll('path')
    											.data(contours[timeIndex], function() { return contourId++; });

		contourLines.enter().append('path')
    		.style('fill', function(d) { return colors(d.level); })
				.style('fill-opacity', 0.0)
				.style('stroke', 'black')
				.style('stroke-opacity', 0.0)
    		.attr('d', d3.svg.line()
      		.x(function(d) { return x(d.x); })
      		.y(function(d) { return y(d.y); }));

		contourLines.transition()
				.delay(animationDuration / 2)
				.duration(animationDuration / 2)
				.style('fill-opacity', 1.0)
				.style('stroke-opacity', 1.0);

		contourLines.exit().transition()
				.duration(animationDuration / 2)
				.style('fill-opacity', 0.0)
				.style('stroke-opacity', 0.0)
				.remove();
	}

	var step = 0;

	interval = setInterval(function() {
		draw(step);
		step += 1;

		if (step >= time.length) {
			clearInterval(interval);
		}
	}, slideDuration);
}

function buildContours(varName, tidyData, time, xAxis, yAxis, contourLevels) {
	var contours = [];

	time.forEach(function(timeVal) {
		var contourBuilder = new Conrec;

		var tidyDataForTime = timeFilter(tidyData, timeVal);
		var griddedData = buildArray(varName, tidyDataForTime, xAxis, yAxis, 'COADSX', 'COADSY', 0.0);

		contourBuilder.contour(griddedData, 0, xAxis.length - 1, 0, yAxis.length - 1, xAxis, yAxis, contourLevels.length, contourLevels);

		var contourList = contourBuilder.contourList();

		contours.push(contourList);
	});

	return contours;
}

function buildArray(varName, tidyData, xAxis, yAxis, xVar, yVar, fillValue) {
	if (fillValue === undefined) {
		var fillValue = null;
	}

	var retArray = [];

	xAxis.forEach(function(xVal, xIndex) {
		retArray[xIndex] = [];

		yAxis.forEach(function(yVal, yIndex) {
			retArray[xIndex][yIndex] = fillValue;
		});
	});

	tidyData.forEach(function(value) {
		var xIndex = xAxis.indexOf(value[xVar]);
		var yIndex = yAxis.indexOf(value[yVar]);

		retArray[xIndex][yIndex] = value[varName];
	});

	return retArray;
}

function buildAxis(tidyData, axisName) {
	var axis = [];

	tidyData.forEach(function(entry) {
		if (axis.indexOf(entry[axisName]) == -1) {
			axis.push(entry[axisName]);
		}
	});

	return axis.sort(function(a, b) {return a - b});
}

function timeFilter(tidyData, timeValue) {
	var retArray = [];

	tidyData.forEach(function(value) {
		if (value.TIME == timeValue) {
			retArray.push(value);
		}
	});

	return retArray;
}
