var baseUrl = 'http://test.pydap.org/coads.nc';

function load() {
	var varName = 'WSPD';

	$.when(
		opendapLoadDatasetDescriptionPromise(baseUrl),
		opendapLoadDataPromise(baseUrl, varName)
	).done(function(ddsData, varData) {
		var tidyData = datamaid.tidyOPeNDAP(varName, ddsData, varData, true);
		plotData(varName, tidyData);
	});
}

function opendapLoadDatasetDescriptionPromise(url) {
	var deferred = $.Deferred();

	jsdap.loadDataset(url, function(data) {
		deferred.resolve(data);
	}, '/proxy/');

	return deferred.promise();
}

function opendapLoadDataPromise(url, variable) {
	var deferred = $.Deferred();

	var dodsUrl = url + '.dods?' + variable;

	jsdap.loadData(dodsUrl, function(data) {
		deferred.resolve(data);
	}, '/proxy/');

	return deferred.promise();
}

//http://bl.ocks.org/mbostock/3048450
function plotData(varName, tidyData) {
	var values = [];
	var maxValue = d3.max(tidyData, function(d) { return d[varName]; });

	//We plot as a percentage of maximum, otherwise D3 seems confused
	tidyData.forEach(function(entry) {
		values.push(entry[varName] / maxValue);
	});

	var width = 960;
	var height = 500;

	var x = d3.scale.linear()
		.domain([0, 1])
		.range([0, width])

	var xAxis = d3.svg.axis()
    .scale(x)
    .orient('bottom');

	//Generate a histogram using twenty uniformly-spaced bins.
	var data = d3.layout.histogram()
		.bins(x.ticks(20))
		(values);

	//We have the bins now, we can figure out the height
	var y = d3.scale.linear()
		.domain([0, d3.max(data, function(d) { return d.y; })])
    .range([height, 0]);

	var svg = d3.select('.chart')
		.attr('width', width)
		.attr('height', height);

	var bar = svg.selectAll('.bar')
		.data(data)
  .enter().append('g')
    .attr('class', 'bar')
    .attr('transform', function(d) { return 'translate(' + x(d.x) + ',' + y(d.y) + ')'; });

	bar.append('rect')
    .attr('x', 1)
    .attr('width', x(data[0].dx) - 1)
    .attr('height', function(d) { return height - y(d.y); });

	svg.append('g')
    .attr('class', 'x axis')
    .attr('transform', 'translate(0,' + height + ')')
    .call(xAxis);
}
