To run these examples you need Python with [Paste](http://pythonpaste.org/), [PasteScript](http://pythonpaste.org/script/) and [PasteDeploy](http://pythonpaste.org/deploy/). You will also need to have compiled datamaid (`npm run build` which will also copy the compiled javascript to the correct folder). You can run the examples by typing:

```sh
$ PYTHONPATH=. paster serve server.conf
```

 This will create a web server at:

    `http://localhost:8888/`

And a proxy at:

    `http://localhost:8888/proxy/`


Demo1 requires an accessible pydap server at the url specified in `src/demo1.js` serving the stocks.csv data from [bl.ocks.org](http://bl.ocks.org/mbostock/1256572)


Open the demos at:

 * [http://localhost:8888/demo0.html](http://localhost:8888/demo0.html) - Simple histogram
 * [http://localhost:8888/demo1.html](http://localhost:8888/demo1.html) - Recreation of the first frames of the D3 Show Reel
 * [http://localhost:8888/demo2.html](http://localhost:8888/demo2.html) - Slideshow of contours, created with [conrec.js](https://github.com/jasondavies/conrec.js)
